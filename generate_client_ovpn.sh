cd /data/easy-rsa
mkdir -p /data/easy-rsa/keys
source ./vars

export CLIENT_NAME=$1

./pkitool $CLIENT_NAME

cat > keys/$CLIENT_NAME.ovpn <<EOF

<ca>
`cat keys/ca.crt`
</ca>

<key>
`cat keys/$CLIENT_NAME.key`
</key>

<cert>
`cat keys/$CLIENT_NAME.crt`
</cert>

EOF

cp keys/$CLIENT_NAME.ovpn /data/.workbench/$CLIENT_NAME.ovpn
