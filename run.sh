mkdir -p /data/.workbench


if [ ! -d /data/easy-rsa ]
then
    cp -rv /usr/share/easy-rsa/ /data/easy-rsa
    rm /data/easy-rsa/vars
fi

if [ ! -f /data/server.conf ]
then
    cp /dockopenvpn/server.conf /data/server.conf
fi

if [ ! -f /data/easy-rsa/vars ]
then
    cp /dockopenvpn/easy-rsa_vars /data/easy-rsa/vars
fi

cd /data/easy-rsa
mkdir -p /data/easy-rsa/keys
source ./vars

if [ -n "$(find /data/.workbench -prune -empty)" ]
then
    ./clean-all
    ./pkitool --initca
    ./pkitool --server server
    cp /data/server.conf /data/.workbench/server.conf
    cp ./keys/ca.crt /data/.workbench/ca.crt
    cp ./keys/server.crt /data/.workbench/server.crt
    cp ./keys/server.key /data/.workbench/server.key
    cp /dockopenvpn/dh2048.pem /data/.workbench/dh2048.pem
fi


iptables -t nat -D POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE

openvpn --config /data/.workbench/server.conf
