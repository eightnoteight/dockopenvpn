FROM ubuntu:14.04
MAINTAINER Srinivas Devaki (mr.eightnoteight@gmail.com)

EXPOSE 1194

VOLUME /data

RUN mkdir /dockopenvpn

COPY run.sh /dockopenvpn/run.sh
COPY server.conf /dockopenvpn/server.conf
COPY easy-rsa_vars /dockopenvpn/easy-rsa_vars


RUN apt-get update && \
    apt-get install openvpn easy-rsa iptables -y && \
    openssl dhparam -out /dockopenvpn/dh2048.pem 2048

CMD ["bash", "/dockopenvpn/run.sh"]


